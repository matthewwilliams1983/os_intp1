#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <semaphore.h>

char *gen_random_string();
void clear_shared_memory();
int get_buffer_slot();
void *produce(void *job_counter);
void validate_args(int argc, char *argv[]);