#include "producer_maw.h"

/* ** ********************************************
 **	NAME: Matthew Williams 
 **	COURSE: CEG 4350 Fall 2017
 **	Project 1: Producer / Consumer Problem
 **	Description: This project uses semaphores and threads to
 **			       to produce messages to a buffer that is designated
 **			       as shared memory.  This project illustrates many
 **			       programming concepts but is mostly to show how
 **			       the schedular works.
 ** **********************************************/

/* Constants */
const char *NAME = "MAW";
const char *HELP_MESSAGE = "\nThis program sets up shared memory and the producer for the producer"
			   "\nconsumer project, project 1.\n\n"
			   "This program can be started correctly in one of three ways..."
			   "\nExample 1 -		./producer_maw"
			   "\nExample 2 -		./producer_maw 10 10 10"
			   "\nExample 3 -		./producer_maw -h"
			   "\n\nExample 2 above shows that you can supply exactly 3 options if"
			   "\nyou want to change the BUFFER_SIZE, NUM_PRODUCERS, and the"
			   "\nNUM_MESSAGES. The first arg passed corresponds to the buffersize,"
			   "\nthe second to the number of producers and the third for the "
			   "\nthe number of messages to produce."
			   "\nNOTE: Values for arguements must be between 1-100 (inclusive)."
			   "\n\nThe default values for the adjustable variables are as follows:"
			   "\nBUFFER_SIZE: 10\nNUM_PRODUCERS: 5\nNUM_MESSAGES: 100"
			   "\n\nTry running the program with no arguements if you are having"
			   "\ndifficulties (./producer_maw).\n\n";

/* User args modify these */
int buffer_size = 10;
int NUM_PRODUCERS = 5;
int NUM_MESSAGES = 100;
int shared_mem_size = 320; /* buffer_size x 32 */

/* Tracking variables for producing */
int total_jobs_completed = 0;
int producer_number_count = 1;

/* Memory pointer declarations */
void *ptr;
void *share_buffer;
void *current_buffer_address;

/* Semaphore declarations */
sem_t *lock;
sem_t *empty;
sem_t *full;

/* File descriptor declarations */
int shm_fd;
int buff_fd;

/* Threading declartions */
pthread_t tid;
pthread_attr_t attr;

/* ** ********************************************
 **	Keeping this here for my own reference... 
 **	Not sure where I got the idea that _msg_
 **	was supposed to be a set of random characters... 
 **	Directions don't say anything remotely close to that...
 ** **********************************************/
char *gen_random_string(){

	srand(time(NULL));
	char *rand_string = (char*)malloc(5);/* I want to be free */

	for(int x = 0; x < 5; x = x + 1){
		rand_string[x] = rand() % 26 + 97;
	}

	return rand_string;
}/**********************************EoF gen_random_string*/

/* ** ********************************************
 **	Description: This function unlinks the memory and the  
 **			      semaphores. Called at the start of execution.
 **
 **	@param n/a
 **	
 **	@return n/a
 **
 **	NOTES: Because memory is unlinked when this program
 **		      runs, you should always have more or the same 
 **		      amount of messages as your consumer. This is not
 **		      ideal but takes care of any unexpected terminations.
 **		      Long story short run the consumer again if you need to
 **		      not the producer.
 **
 ** **********************************************/
void clear_shared_memory(){

	printf("Clearing shared memory before starting producers...\n");

	/* unlink memory */
	sem_unlink("LOCK");
	sem_unlink("EMPTY");
	sem_unlink("FULL");
	shm_unlink("BUFF_SIZE");
	shm_unlink("MAW");

	printf("Shared memory unlinked...\n");

}/**********************************EoF clear_shared_memory*/

/* ** ********************************************
 **	 
 **	Description: gets the buffer "slot" that should be written to
 **			      written to next.
 **	
 **	@param n/a
 **
 **	@return buffer_slot int - The buffer slot that should be written
 **						   to by the current thread. e.g. 7
 **	
 ** **********************************************/
int get_buffer_slot(){

	int buffer_slot;

	/* Bit hacky but works... Compute the buffer slot */
	if ((total_jobs_completed + 1) % buffer_size == 0){
		buffer_slot = buffer_size;
	}
	else{
		buffer_slot = (total_jobs_completed + 1) % buffer_size;
	}

	return buffer_slot;
}/**********************************EoF get_buffer_slot*/

/* ** ********************************************
 **	 
 **	Description: Produces (writes) to shared memory while 
 **			      while the thread has the lock and message
 **			      still need to be written.
 **
 **	@param job_counter void* - This value is used to keep track
 **							   of how many jobs each producer
 **							   has completed.
 **
 **	@return n/a
 **	
 **	
 ** **********************************************/
void *produce(void *job_counter){
	
	/* Keep track of number of jobs completed by thread*/
	int current_job_num = *((int *)job_counter);

	/* Keep track of the thread "number" */
	int producer_number = producer_number_count;
	producer_number_count++;

	/* Keep thread working as long as there are more messages to produce */
	while(NUM_MESSAGES > 0){
		
		/* Entry section */
		sem_wait(empty);
		sem_wait(lock);

		/* Critical Section */
		if (NUM_MESSAGES > 0){

			/* Get the buffer slot the message should go into */
			int buffer_slot = get_buffer_slot(); 
 
			/* Write message to buffer slot */
			char message[32];
			sprintf(message, "%lu_msg%i", pthread_self(), current_job_num);
			current_buffer_address = ptr + (32 * (buffer_slot - 1));
			sprintf(current_buffer_address, "%s", message);

			total_jobs_completed++;

			/* Print out what happend above to give visual feedback */
			printf("Producer %i produced %lu_msg%i on buffer %i		PJC: %i\n", producer_number, pthread_self(), 
				    current_job_num, buffer_slot, total_jobs_completed);
			/* Note: pthread_self() is the thread Id I think but lecture says it should be 4 or 5 in length idk */

			/* Decrement num messages to know when the threads should stop producing */
			NUM_MESSAGES--;
			/* Just keep tracking of the total number of jobs each producer has done */
			current_job_num++;
		}
		sem_post(lock);
		sem_post(full);
		/* Exit */
		/* Remainder */
		sleep(1);
	}
	pthread_exit(0);
}/**********************************EoF producer*/

/* ** ********************************************
 **	 
 **	Description: Validates the arguements passed to the program.
 **	
 **	@param argc int - the number of arguements passed from the
 **					command line.
 **
 **	@param argv[] char* - the arguements.
 **
 **	@return n/a
 **	
 ** **********************************************/
void validate_args(int argc, char *argv[]){

	/* The user has entered the -h option for help */
	if (argc == 2 && strcmp(argv[1], "-h") == 0){
		printf("%s", HELP_MESSAGE);
		exit(0);
	}
	/* The user supplied the incorrect amount of args */
	if (argc != 4 && argc != 1){
		printf("\nThe number of arguements supplied %i is not valid.", argc - 1);
		printf("\n%s", HELP_MESSAGE);
		exit(0);
	}
	/* The user has specificed 3 arguements, validate them below */
	else if (argc == 4){
		buffer_size = atoi(argv[1]);
		NUM_PRODUCERS = atoi(argv[2]);
		NUM_MESSAGES = atoi(argv[3]);
		if (buffer_size > 100 || buffer_size < 1){
			printf("\nBUFFER_SIZE of %i is not valid.", buffer_size);
			printf("\n%s", HELP_MESSAGE);
			exit(0);
		}
		else if (NUM_PRODUCERS > 100 || NUM_PRODUCERS < 1){
                        printf("\nNUM_PRODUCERS of %i is not valid.", NUM_PRODUCERS);
                        printf("\n%s", HELP_MESSAGE);
			exit(0);
                }
		else if (NUM_MESSAGES > 100 || NUM_MESSAGES < 1){
                        printf("\nNUM_MESSAGES of %i is not valid.", NUM_MESSAGES);
                        printf("\n%s", HELP_MESSAGE);
			exit(0);
                }
	}
}/**********************************EoF validate_args*/

/* ** ********************************************
 **	 
 **	Description: Entry point.
 **	
 **	@param argc int - the number of arguements passed from the
 **					command line.
 **
 **	@param argv[] char* - the arguements.
 **
 **	@return 0 - program exectued without error.
 **	
 ** **********************************************/
int main(int argc, char *argv[]){

	/* Unlink memory before starting */
	clear_shared_memory();
	
	/* Validate the arguements supplied form the command line */
	validate_args(argc, argv);
	
	/* Create lock and semaphores */
	lock = sem_open("LOCK", O_CREAT, 0666, 1);
	empty = sem_open("EMPTY", O_CREAT, 0666, buffer_size);
	full = sem_open("FULL", O_CREAT, 0666, 0);

	/* Share buffer size with consumer */
	buff_fd = shm_open("BUFF_SIZE", O_CREAT | O_RDWR, 0666);
	ftruncate(buff_fd, sizeof(int));
	share_buffer = mmap(0, sizeof(int), PROT_WRITE, MAP_SHARED, buff_fd, 0);
	sprintf(share_buffer, "%i", buffer_size);
	
	/* Shared memory to write messages to */
	shm_fd = shm_open(NAME, O_CREAT | O_RDWR, 0666);
	ftruncate(shm_fd, (shared_mem_size * buffer_size));
	ptr = mmap(0, (shared_mem_size * buffer_size), PROT_WRITE, MAP_SHARED, shm_fd, 0);
	pthread_attr_init(&attr);

	/* Create threads or producers */
	int job_counter = 1;
	for (int x = 0; x < NUM_PRODUCERS; x = x + 1){
		pthread_create(&tid, &attr, produce, (void *) &job_counter);
	}

	/* Join threads as they finish */
	pthread_join(tid, NULL);

	return 0;

}/**********************************EoF main*/
/**************************************END*/
