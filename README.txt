NAME: Matthew Williams
CLASS: CEG 4350 Fall 2017
PROJECT: Producer / Consumer Project 1


COMPILE INSTRUCTIONS
gcc -o producer_maw producer_maw.c -pthread -lrt
gcc -o consumer_maw consumer_maw.c -pthread -lrt


NOTES:

* You must supply 3 arguements for the producer file if you want to supply arguements. Type (./producer_maw -h) with no () for help.
* You must supply 2 arguements for the consumer file if you want to supply arguements. Type (./consumer_maw -h) with no () for help.

* The producer file sets up the shared memory and locks and should be run prior to executing the consumer.  If you do not do this the consumer file will prompt you to do so.

* Because the producer file unlinks the memory at the start of execution, you should have your messages to produce to >= to the consumer messages.
Not doing so will result in the consumer waiting to consume more messages and if the producer is ran again it will unlink the shared memory.
This is done to easily reset the shared memory to quickly test different parameters for the project. My suggestion...
Run the producer where Messages > BufferSize
Run the consumer where Consumer Messages < Producer Messages
Run the consumer again and again untill you've consumed exactly the number of producer messages
If you do not do this you will have to disable the mem unlink in the producer or cntl c the consumer and restart the producer.

* The above notes was the best way to do this project as far as I can tell...