#include "consumer_maw.h"

/* ** ********************************************
 **	NAME: Matthew Williams 
 **	COURSE: CEG 4350 Fall 2017
 **	Project 1: Producer / Consumer Problem
 **	Description: This project uses semaphores and threads to
 **			       to consume messages to a buffer that is designated
 **			       as shared memory.  This project illustrates many
 **			       programming concepts but is mostly to show how
 **			       the schedular works.
 ** **********************************************/

/* Constants */
const char *NAME = "MAW";
const char *HELP_MESSAGE = "\nThis program sets up and runs the consumer for the producer"
						      "\nconsumer project, project 1.\n\n"
						      "This program can be started correctly in one or three ways..."
						      "\nExample 1 - 	./consumer_maw"
						      "\nExample 2 -	./consumer_maw 10 10"
						      "\nExample 3 -	./consumer_maw -h"
						      "\n\nExample 1 is the default way to run the program..."
						      "\n\nExample 2 above shows that you can supply exactly 2 options if"
						      "\nyou want to change the NUM_CONSUMERS and the NUM_MESSAGES."
						      "\nThe first arg passed corresponds to the number of consumers, the second"
						      "\nto the number of messages to consume."
						      "\nNote: Values for arguments must be between 1-100(inclusive)."
						      "\n\nExample 3 uses the -h option to pull up this help message."
						      "\n\nThe default values for the adjustable variables are as follows:"
						      "\nNUM_CONSUMERS: 5\nNUM_MESSAGES: 100"
						      "\n\nTry running the program with no arguements if you are having"
						      "\ndifficulties (./consumer_maw).\n"
						      "\n\nFINAL NOTE: Ensure that the producer program has been run before"
						      "\nrunning this program. The execution relies on the shared memory setup"
						      "\nfrom the producer program.\n\n";

/* Needs to be read from shared */
int buffer_size;
int shared_mem_size = 320;
/* User args modify these */
int NUM_CONSUMERS = 5;
int NUM_MESSAGES = 100;

/* Tracking variables for consuming */
int total_jobs_completed = 0;
int consumer_number_count = 1;

/* Memory pointer declarations */
void *ptr;
void *buff_size_map;
void *current_buffer_address;

/* Semaphore declarations */
sem_t *lock;
sem_t *empty;
sem_t *full;

/* File descriptor declarations */
int shm_fd;
int buff_fd;

/* Threading declartions */
pthread_t tid;
pthread_attr_t attr;

/* ** ********************************************
 **	 
 **	Description: gets the buffer "slot" that should be written to
 **			      written to next.
 **	
 **	@param n/a
 **
 **	@return buffer_slot int - The buffer slot that should be written
 **						   to by the current thread. e.g. 7
 **	
 ** **********************************************/
int get_buffer_slot(){

	int buffer_slot;

	/* Bit hacky but works... Compute the buffer slot */
	if ((total_jobs_completed + 1) % buffer_size == 0){
		buffer_slot = buffer_size;
	}
	else{
		buffer_slot = (total_jobs_completed + 1) % buffer_size;
	}

	return buffer_slot;
}/**********************************EoF get_buffer_slot*/

/***********************************************
 **	 
 **	Description: This function consumes from the buffer.
 **
 **	@pram job_counter void * - Used to tally number of jobs each
 **							  consumer does. Not used.
 **
 **	@return n/a
 **	
 ** **********************************************/
void *consumer(void *job_counter){

	/* Keep track of number of jobs completed by thread */
	/* Really don't need this as it is part of the producer msg portion and will make print statements "ugly" */
	/* It's here anyway if I want to add it in (I don't though)... */
	int current_job_num = *((int *)job_counter);
	
	/* Keep track of the thread "number" */
	int consumer_number = consumer_number_count;
	consumer_number_count++;

	/* Keep thread working as long as messages to consume specifies */
	while(NUM_MESSAGES > 0){

		/* Entry section */
		sem_wait(full);
		sem_wait(lock);

		/* Critical Section */
		if(NUM_MESSAGES > 0){

			/* Figure out where we need to read from */
			int buffer_slot = get_buffer_slot();
			current_buffer_address = ptr + (32 * (buffer_slot - 1));

			if( (char*) current_buffer_address != NULL){
				
				/* "Consume */
				total_jobs_completed++;
				printf("Consumer %i consumed %s from buffer %i		CJC: %i\n", consumer_number, 
				(char *) current_buffer_address, buffer_slot, total_jobs_completed);
				/* This print will be out of line cause its a string we are using instead of a long unsigned */

				/* Keep track of how many more messages need to be consumed and completed */
				NUM_MESSAGES--;
			}
		}
		sem_post(lock);
		sem_post(empty);
		/* Exit */
		/* Remainder */
        	sleep(1);
	}
	pthread_exit(0);
}/**********************************EoF consumer*/

/* ** ********************************************
 **	 
 **	Description: Validates the arguements passed to the program.
 **	
 **	@param argc int - the number of arguements passed from the
 **					command line.
 **
 **	@param argv[] char* - the arguements.
 **
 **	@return n/a
 **	
 ** **********************************************/
void validate_args(int argc,  char *argv[]){
	
	/* The user has entered the -h option for help */
	if (argc == 2 && strcmp(argv[1] , "-h") == 0){
		printf("%s", HELP_MESSAGE);
		exit(0);
	}
	/* The user supplied the incorrect amount of args */
	else if (argc != 3 && argc != 1){
		printf("\nThe number of arguements supplied %i is not valid.", argc - 1);
		printf("\n%s", HELP_MESSAGE);
		exit(0);
	}
	else if (argc == 3){
		NUM_CONSUMERS = atoi(argv[1]);
		NUM_MESSAGES = atoi(argv[2]);
		if (NUM_CONSUMERS > 100 || NUM_CONSUMERS < 1){
			printf("\nNUM_CONSUMERS of %i is not valid.", NUM_CONSUMERS);
                        printf("\n%s", HELP_MESSAGE);
			exit(0);
		}
		else if (NUM_MESSAGES > 100 || NUM_MESSAGES < 1){
                        printf("\nNUM_MESSAGES of %i is not valid.", NUM_MESSAGES);
                        printf("\n%s", HELP_MESSAGE);
			exit(0);
                }
	}
}/**********************************EoF validate_args*/

/***********************************************
 **	 
 **	Description: This function checks if producer has been run by
 **			       attempting to access the lock semaphore.
 **
 **	@pram n/a
 **
 **	@return n/a
 **	
 ** *********************************************/
void validate_producer(){

	if (sem_open("LOCK", 0) == SEM_FAILED){
		printf("\nProducer needs to be executed first.\nShared memory not created...");
		printf("\n%s", HELP_MESSAGE);
		exit(0);
	}
}/**********************************EoF validate producer*/

/***********************************************
 **	 
 **	Description: Entry point.
 **	
 **	@param argc int - the number of arguements passed from the
 **					command line.
 **
 **	@param argv[] char* - the arguements.
 **
 **	@return 0 - program exectued without error.
 **	
 ************************************************/
int main(int argc, char *argv[]){
	
	/* Validate the command line args */
	validate_args(argc, argv);

	/* Verify the producer has setup shared memory */
	validate_producer();
	
	/* Setup lock and semaphores */
	lock = sem_open("LOCK", 0);
	empty = sem_open("EMPTY", 0);
	full = sem_open("FULL", 0);

	/* Get the buffer size from the producer */
	buff_fd = shm_open("BUFF_SIZE", O_RDONLY, 0666);
	buff_size_map = mmap(0, 32, PROT_READ, MAP_SHARED, buff_fd, 0);
	buffer_size = atoi((char *) buff_size_map);

	/* Setup the shared memory to consume from */
	shm_fd = shm_open(NAME, O_RDONLY, 0666);
	ptr = mmap(0, (shared_mem_size * buffer_size), PROT_READ, MAP_SHARED, shm_fd, 0);
	pthread_attr_init(&attr);

	/* Create threads or consumers */
	int job_counter = 1;
	for(int x = 0; x < NUM_CONSUMERS; x = x + 1){
		pthread_create(&tid, &attr, consumer, (void *) &job_counter);
	}

	/* Join threads as they finish */
	pthread_join(tid, NULL);

	return 0;
}/**********************************EoF main*/
/**************************************END*/
