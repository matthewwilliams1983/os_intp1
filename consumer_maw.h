#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <semaphore.h>

int get_buffer_slot();
void *consumer(void *job_counter);
void validate_args(int argc,  char *argv[]);
void validate_producer();